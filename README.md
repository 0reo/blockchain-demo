# Blockchain Demo

This project is an attempt to make and visualize a blockchain(and possibly other structures) in javascript for educational purposes.  

#### To use

Run 'npm install' after checkout to initialize the project.

This is currently a work in progress, and does not yet have a development branch(soon though).  So things may not work currently.
That said, the available scripts are as follows

```
    "start-backend": "nodemon ./bin/www --workspace=backend",
    "tree-backend": "node ./bin/merkleTree.js --workspace=backend",
    "trie-backend": "node ./bin/patriciaTrie.js --workspace=backend",
    "run-backend": "node ./bin/main.js --workspace=backend",
    "start-react": "npm start --workspace=frontend",
    "build-react": "npm build --workspace=frontend",
    "test-react": "npm test --workspace=frontend",
    "eject-react": "npm eject --workspace=frontend"
```

- `run-backend` is currently used to test the trees via command line.
- backend scripts currently take 2 optional arguments, one for setting the number of random entries to use, and
one for turning on debug logs
  - `npm run tree-backend 22 true`
- the react scrips are to start the front end, and are not yet well connected to the backend(or at all connected).

Currently the only requirements are node/npm, but as development continues running `npm install` may be required.
