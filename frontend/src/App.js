import logo from './logo.svg';
import './App.css';
import { Box } from "./components"
import { Canvas } from '@react-three/fiber'
// import {Center} from '@react-three/drei'
import { useEffect, useState, createContext, useContext } from "react";

const context = createContext();

function App() {

  const [merkleTree, setMerkleTree] = useState();
  const [boxes, setBoxes] = useState([]);
  const numBlocks = 2**3;
  let size = 0.25;
  const geometry = <boxGeometry args={[5, 10, 1]}/>;
  // const geometry = null;

  useEffect(() => {
    if (!merkleTree){
      fetch(`http://localhost:3001/merkleTree?max=${numBlocks}`)
        .then((response) => response.json())
        .then((responseJson) => {
          setMerkleTree(responseJson);

        }).catch((e)=>{
          console.log(e);
        })
    }

  });

  useEffect(()=>{
    if (!merkleTree || boxes.length) return;

    const scale = window.screen.width/window.screen.height;

    setBoxes(
      merkleTree.map((data, i) => {
        return <Box
          geometry={geometry}
          index={i}
          key={`box-${i}`}
          max={numBlocks}
         />
      })
    )
  },[merkleTree, boxes])



  return (
    <Canvas
    style={{background:'grey'}}
    // camera={{zoom:70}}
    // orthographic
    >

      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      {boxes}

    </Canvas>
  );
}

export default App;
