import ReactDOM from 'react-dom'
import React, { useRef, useState } from 'react'
import { useFrame, useThree } from '@react-three/fiber'

export default function Box(props) {
  // This reference gives us direct access to the THREE.Mesh object
  const ref = useRef()
  const sceneState = useThree();
  const midScreen = () => [sceneState.viewport.width / 2, sceneState.viewport.height / 2];

  // Hold state for hovered and clicked events
  const [hovered, hover] = useState(false)
  const [clicked, click] = useState(false)
  // Subscribe this component to the render-loop, rotate the mesh every frame
  useFrame((state, delta) => (ref.current.rotation.x += 0.01))

  const position = [];
  const index = props.index;
  const limit = Math.cbrt(props.max);

  position[0] = (index%limit);
  // position[0] -= midScreen()[0];
  position[1] = (Math.floor((index)/limit)%limit)-(midScreen()[1]/4);
  position[2] = Math.floor(index/Math.pow(limit, 2))-(1);

  console.log(limit, midScreen());

  // Return the view, these are regular Threejs elements expressed in JSX
  return (
    <mesh
      {...props}
      position={position}
      ref={ref}
      scale={clicked ? 1.5 : 0.5}
      onClick={(event) => click(!clicked)}
      onPointerOver={(event) => hover(true)}
      onPointerOut={(event) => hover(false)}>
      {props.geometry}
      // <boxGeometry args={[0.5,0.5,0.5]}/>
      <meshStandardMaterial color={hovered ? 'hotpink' : 'orange'} />
    </mesh>
  )
}
//
// 0,0,0
// 0,0,1
// 0,1,0
// 0,1,1
//
//
// 1,0,0
// 1,0,1
// 1,1,0
// 1,1,1
