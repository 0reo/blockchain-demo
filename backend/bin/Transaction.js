const { getHash, compareHash, signMessage } = require ("./utils")
// const NonceGenerator = require('a-nonce-generator');


class Transaction {
  constructor(to=0, from=0, payload=0, nonce=0, privateKey=null) {
    // const ng = new NonceGenerator()


    this.to = to;
    this.from = from;
    this.payload = payload;
    this.r = privateKey.export({format:"jwk"}).x;
    this.s = privateKey.export({format:"jwk"}).y;
    this.id = nonce;
    this.signature = signMessage(this.hash, privateKey)

    // console.log(`SIGNED: ${this.signature}`);

    // Calculate a hash (e) from the message to sign.
    // Generate a secure random value for k.
    // Calculate point (x₁, y₁) on the elliptic curve by multiplying k with the G constant of the elliptic curve.
    // Calculate r = x₁ mod n. If r equals zero, go back to step 2.
    // Calculate s = k⁻¹(e + rdₐ) mod n. If s equals zero, go back to step 2.
  }

  get data() {
    return this.to+this.from+this.payload
  };

  get hash() {
    return getHash(this.data);
// return getHash(`${this.id}`);
  }

  add(_data) {
  }

  remove(_data){
  }


  verifyData(_data) {
  }
}

module.exports = Transaction;
