const { getHash, compareHash } = require ("./utils")
const readline = require('readline');
const DEBUG = process.argv[3] || false;

const printDebug = (_msg) => {
  //assert prints on true, but DEBUG would be inverse of what we want if we used
  //it directly, so we check for the inverse of DEBUG instead
  DEBUG && console.log(_msg);
};

class MerkleTree {
  constructor() {
    this.layers = [[]];
    this.baseLayerIndex = 0;
    this.baseLayer = this.layers[this.baseLayerIndex];
    this.numLayers = () => this.layers.length;
    this.salt = 10;
  }

  get merkleRoot() {
    return this.layers[0][0];
  }

  addLeaf(_leaf) {
    printDebug(`\n\nstart addLeaf: ${_leaf}`);
    const newLeaf = { data: _leaf.data, hash: _leaf.hash };

    newLeaf.hash = getHash(newLeaf.data);

    this.baseLayer.push(newLeaf);

    for (
      let layerIndex = this.layers.length - 1;
      layerIndex >= 0;
      layerIndex--
    ) {
      const layer = this.layers[layerIndex];
      const nextLayerIndex = layerIndex - 1;
      printDebug(
        `Entering layer ${layerIndex}, tree has ${this.layers.length} layers, ${
          this.baseLayer.length
        } leaves, newest leaf is ${_leaf}\n\n${JSON.stringify(layer)}`
      );

      for (const [nodeIndex, node] of layer.entries()) {
        const isLastNode = nodeIndex == layer.length - 1;
        const isPair = nodeIndex % 2;
        const isBaseLayer = layer == this.baseLayer;

        //if we're on the base layer, we only need to work with the newest leaf
        if (isBaseLayer && !isLastNode) {
          printDebug(`${layerIndex} ${nodeIndex} Skipping`);
          continue;
        }
        printDebug(
          `layer: ${layerIndex} node: ${node.data} length: ${layer.length}`
        );

        const prevNodeIndex = nodeIndex - 1;
        const halfIndex = Math.floor(nodeIndex / 2);
        const prevNode = layer[prevNodeIndex];
        const nextLayer = this.layers[nextLayerIndex];
        const targetNode = nextLayer && nextLayer[halfIndex];

        let match;

        //if you're at the end of the layer, and it has an odd number of items, and thre's another layer
        if (isLastNode && !isPair) {
          printDebug(
            `${layerIndex} ${nodeIndex}: attempt to add LAST node(${node.data}) to next layer(${nextLayerIndex})`
          );

          //add node to next layer up if layer exists and we're on the base layer
          if (targetNode) {
            printDebug(
              `${layerIndex} ${nodeIndex}: checking hash for node ${halfIndex} on layer ${nextLayerIndex}`
            );

            match = node.hash == targetNode.hash;
            if (match) {
              printDebug(
                `${layerIndex} ${nodeIndex}: node ${halfIndex} on layer ${nextLayerIndex} is valid, no need to update. Match = ${match}`
              );
            } else {
              printDebug(
                `${layerIndex} ${nodeIndex}: target node is outdated, updating node ${halfIndex} on layer ${nextLayerIndex}`
              );
              nextLayer[halfIndex] = node;
            }
          } else if (nextLayer) {
            printDebug(
              `${layerIndex} ${nodeIndex}: adding leaf to next layer ${nextLayerIndex}`
            );
            this.addNodeToLayer(nextLayerIndex, node);
          }
        } else if (isPair) {
          printDebug(
            `${layerIndex} ${nodeIndex}: prepping nodes ${prevNodeIndex} and ${nodeIndex}(${
              prevNode.data
            }, ${node.data}) for next layer ${
              nextLayerIndex < 0 ? "" : nextLayerIndex
            }`
          );

          //add to next layer up if layer exists, or make new layer with new node
          if (nextLayer) {
            printDebug(
              `${layerIndex} ${nodeIndex}: attemping to add new node layer ${nextLayerIndex}: currently has ${nextLayer.length} nodes`
            );

            printDebug(
              `${layerIndex} ${nodeIndex}: comparing hash on node ${halfIndex} on layer ${nextLayerIndex}: ${
                targetNode && targetNode.data
              }`
            );

            match =
              targetNode &&
              compareHash(`${prevNode.hash}${node.hash}`, targetNode.hash);
            if (match) {
              printDebug(
                `${layerIndex} ${nodeIndex}: node ${halfIndex} on layer ${nextLayerIndex} is valid, no need to update. Match = ${match}`
              );
            } else {
              printDebug(
                `${layerIndex} ${nodeIndex}: Match 2 is ${match}, updating node ${halfIndex} on layer ${nextLayerIndex}`
              );
              nextLayer[halfIndex] = this.mergeNodes(prevNode, node);
            }
          } else {
            printDebug(
              `${layerIndex} ${nodeIndex}: making new layer ${nextLayerIndex}`
            );
            this.addNewLayer(prevNode, node);
          }
        }
        printDebug(
          `${layerIndex} ${nodeIndex}: finished with node ${
            node.data
          }, the next layer ${nextLayerIndex} now has ${
            nextLayer && nextLayer.length
          } nodes`
        );
      }
      printDebug(`${layerIndex}: exiting layer`);
    }
  }

  //add new layer above all other layers
  //merkle root should be 0, leaves should be bottom layer
  addNewLayer(_node1, _node2) {
    this.layers.unshift([this.mergeNodes(_node1, _node2)]);
    this.baseLayerIndex = this.layers.length - 1;
    this.baseLayer = this.layers[this.baseLayerIndex];
  }

  addNodeToLayer(_layerIndex, _node) {
    printDebug(`-- adding node ${_node.data} to next layer ${_layerIndex}`);
    this.layers[_layerIndex].push(_node);
  }

  mergeNodes(_leaf1, _leaf2) {
    const node = {
      hash: "",
      prevHash: { h1: _leaf1.hash, h2: _leaf2.hash },
    };
    if (DEBUG) node.data = `${_leaf1.data}, ${_leaf2.data}`;

    node.hash = getHash(`${_leaf1.hash}${_leaf2.hash}`);
    printDebug(`-- mergeNodes returning new node: ${node.data}`);
    return node;
  }

  viewTree() {
    console.log("\n\n\nFull tree", JSON.stringify(this.layers, null, 4));

    console.log(`\n\n\nTree has ${this.layers.length} layers`);
    this.layers.forEach((layer, i) => {
      console.log(`Layer ${i} has ${layer.length} nodes`);
      if (DEBUG && !i) console.log(`Root node hash: ${layer[0].data}`);
    });
  }

  verifyData(_data) {
    if (this.baseLayer.length <= 1) {
      console.log(`not enough leaves in tree to verify data`);
      return;
    }

    if (_data.data) _data = _data.data;

    let nodeId;
    let activeLeaf;
    let sibling;
    let parentHash;

    //find leaf/node pairs to work way through tree
    //loop backwards from leaves to merkle root
    for (var layerId = this.numLayers() - 1; layerId >= 0; layerId--) {
      printDebug(
        `\n\nVerifying ${
          parentHash ? "submitted hash " + parentHash : "input data " + JSON.stringify(_data, null, '\t')
        } on ${layerId > 0 ? "layer " + layerId : "merkle root"}`
      );

      //this is where we actually check if our hash, or our input data, is valid
      const layer = this.layers[layerId];
      activeLeaf = layer.find((node, index) => {
        nodeId = index;
        if (node.data == _data) {
          if (!compareHash(_data, node.hash)) {
            console.log(
              "The hash for the inputted data and the matching leaf do not match.  Exiting."
            );
            printDebug(
              `input data hash: ${getHash(_data)}\n\nleaf hash ${
                node.hash
              }`
            );
            return;
          }
          printDebug("found leaf match for data");
        }
        if (node.hash == parentHash) {
          printDebug(
            `found matching hash on ${
              layerId > 0 ? "layer " + layerId : "merkle root"
            }`
          );
        }
        return node.data == _data || node.hash == parentHash;
      });

      if (!activeLeaf) {
        console.log(
          `INVALID INPUT, no match found for ${_data} on layer ${layerId}`
        );
        printDebug(
          `nodeId = ${nodeId}, layerId = ${layerId}, num nodes = ${
            layer.length - 1
          }, total layers = ${this.numLayers()}`
        );
        return;

        //are we working with a solo node?
      } else if (nodeId == layer.length - 1 && nodeId % 2 == 0) {
        if (layerId > 0) {
          printDebug(
            `found match for ${
              parentHash ? "hash " + parentHash : "data " + _data
            }, but it has no sibling node.`
          );
          if (parentHash) {
            printDebug(
              `reusing hash, validating with parent node on ${
                layerId - 1 ? "layer " + (layerId - 1) : "merkle root"
              }`
            );
          } else {
            parentHash = getHash(_data);
            printDebug(
              `generated new parent hash for single child, validating with parent node on ${
                layerId - 1 ? "layer " + (layerId - 1) : "merkle root"
              }`
            );
          }

          printDebug(`parentHash = ${parentHash}`);
        } else {
          console.log(`On Merkel root`);
          console.log(
            layer[0].hash == parentHash
              ? `Successfully validated input "${_data}" with Merkel root!`
              : `Validation FAILED, hash for "${_data}" did not match Merkel root\n Data hash: XX\n Root hash: ${parentHash}`
          );
          printDebug(
            `parentNode = ${JSON.stringify(layer[0])}, layerId = ${layerId}`
          );
          return;
        }
        printDebug(`activeLeaf = ${JSON.stringify(activeLeaf)}`);

        //comparing 2 sibling nodes to a parent
      } else if (nodeId < layer.length) {
        printDebug(
          `found sibling node on ${layerId}, validating with parent node`
        );
        printDebug(`activeLeaf = ${JSON.stringify(activeLeaf)}`);

        //sibling is to the left if last node or activeLeaf is in odd position
        if (nodeId % 2) {
          sibling = layer[nodeId - 1];
        } else {
          sibling = layer[nodeId + 1];
        }

        let childHashs;
        if (nodeId % 2) {
          childHashs = `${sibling.hash}${activeLeaf.hash}`;
        } else {
          childHashs = `${activeLeaf.hash}${sibling.hash}`;
        }

        parentHash = getHash(childHashs);

        printDebug(
          `generated new parent hash, searching for match on ${
            layerId - 1 ? "layer " + (layerId - 1) : "merkle root"
          }`
        );
        printDebug(`childHashs = ${childHashs}, parentHash = ${parentHash}`);
      }
    }
  }

  addLeaves(_data, _callback) {
    const data = [].concat(_data);

    console.log(`adding ${data.length} items to tree, please wait`);
    for (const [i, item] of data.entries()) {
      if (_callback)
        _callback(item);
      else {
        printDebug(`no callback provided, adding item as leaf`)
        this.addLeaf(item);
      }
      readline.clearLine();
      readline.cursorTo(process.stdout, 0);
      process.stdout.write(
        `${Math.round(((i + 1) / data.length) * 100)}% ` +
          ".".repeat(Math.round(((i + 1) / data.length) * 100))
      );
    }
    process.stdout.write(`\n\n`);
  }
}

module.exports = MerkleTree;
