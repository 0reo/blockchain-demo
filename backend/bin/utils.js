const {
  createHash,
  generateKeyPairSync,
  createSign,
  createVerify,
  getHashes
} = require("crypto");

const crypto = require("crypto")

exports.genKeyPair = (_alg='ec') => {
  return generateKeyPairSync(_alg, {
    modulusLength: 2048,
    namedCurve:'secp256k1',
    // privateKeyEncoding:{type:'pkcs8', format:'der'},
    // publicKeyEncoding:{type:'spki', format:'der'}
  });
}

exports.signMessage = (_message, _privKey = null, _alg='SHA256') => {
  if (!_privKey){
    const keys = this.genKeyPair();
    _privKey = keys.privateKey
    console.log(`Generated key pair because non was provided.  COPY THIS DATA \n `)
    console.log(`${keys.publicKey}`)

    console.log(`${keys.privateKey}`)
  }
  const sign = createSign(_alg);
  sign.update(_message);
  sign.end();
  return sign.sign({key:_privKey, format:'jwk', type:'pkcs1'}).toString('hex');
}

exports.verifyMessage = (_message, _pubKey, _signature, _alg='SHA256') => {
  if (!_pubKey){
    console.log(`Public key is required to verify this message`);
    return false;
  } else if(!_signature){
    console.log(`A signature is required to verify this message`);
    return false;
  }

  const verify = createVerify(_alg);
  verify.update(_message);
  verify.end();
  return verify.verify(_pubKey, _signature);
}

exports.getHash = (_data, _alg='SHA256') => {
  if (_data.hash) return _data.hash;
  return crypto.createHash(_alg).update(_data).digest("hex");
}

exports.compareHash = (_data, _hash, _alg='SHA256') => {
  return this.getHash(_data, _alg) == _hash;
}
