const MerkleTree = require ("./merkleTree");
const PatriciaTrie = require ("./patriciaTrie");
const Transaction = require ("./Transaction");
const {genKeyPair} = require("./utils")

const max = process.argv[2] || 1 + Math.floor(Math.random() * 10000);

// const transactionTrie = new PatriciaTrie();
const stateTrie = new PatriciaTrie();

const transactionList = [];
const storageTries = [];

const account = [];
const blocks = [];
function makeAccounts(_numAccounts = 1){
  account.push({
    address:'',
    nonce:0,
    balance:0,
    codeHash:null,
    storageRoot:null,
    makeTransaction: function() {
      let transaction = new Transaction(
        "sending to",               //to
        this.address,            //from
        `payload ${Math.random()
          .toString(36)
          .replace(/[^a-z]+/g, "")
          .substr(0, 5)
        }`,
        this.nonce,
        this.keys.privateKey
      );
      this.nonce += 1;
      return transaction;
    },
  });

  for (let i = 0; i < _numAccounts; i++){
    account.push( Object.assign({}, account[i]) );
    storageTries.push(new PatriciaTrie());
    account[i].storageRoot = storageTries[i].hash;
  }
}

makeAccounts(100);


const der_pub = {format:"der", type:'spki'};

for (const i in account){
  account[i].keys = genKeyPair();
  account[i].address = account[i].keys.publicKey.export(der_pub).toString('hex').slice(-20);
}


const getAccountRandom = (_accounts) => _accounts[Math.floor(Math.random() * _accounts.length)];

// for (let i = 0; i < max; i++) {
//   transactionList.push(
//     getAccountRandom(account).makeTransaction()
//   );
//
//   if ((i+1) % 10 == 0) {
//     let transactionTree = new MerkleTree();
//     transactionTree.addLeaves(transactionList.slice(-10));
//     blocks.push(transactionTree);
//   }
// }

// transactionTree.addLeaves(transactionList);
// transactionTree.addLeaves(blocks.slice(-1));

stateTrie.addLeaves(account, "address");
// transactionTrie.addLeaves(transactionList, "from");

// transactionTree.merkleRoot.hash


/*


6.2.1.2.  "x" (X Coordinate) Parameter

   The "x" (x coordinate) parameter contains the x coordinate for the
   Elliptic Curve point.  It is represented as the base64url encoding of
   the octet string representation of the coordinate, as defined in
   Section 2.3.5 of SEC1 [SEC1].  The length of this octet string MUST
   be the full size of a coordinate for the curve specified in the "crv"
   parameter.  For example, if the value of "crv" is "P-521", the octet
   string must be 66 octets long.

6.2.1.3.  "y" (Y Coordinate) Parameter

   The "y" (y coordinate) parameter contains the y coordinate for the
   Elliptic Curve point.  It is represented as the base64url encoding of
   the octet string representation of the coordinate, as defined in
   Section 2.3.5 of SEC1 [SEC1].  The length of this octet string MUST
   be the full size of a coordinate for the curve specified in the "crv"
   parameter.  For example, if the value of "crv" is "P-521", the octet
   string must be 66 octets long.

6.2.2.  Parameters for Elliptic Curve Private Keys

   In addition to the members used to represent Elliptic Curve public
   keys, the following member MUST be present to represent Elliptic
   Curve private keys.

6.2.2.1.  "d" (ECC Private Key) Parameter

   The "d" (ECC private key) parameter contains the Elliptic Curve
   private key value.  It is represented as the base64url encoding of
   the octet string representation of the private key value, as defined
   in Section 2.3.7 of SEC1 [SEC1].  The length of this octet string
   MUST be ceiling(log-base-2(n)/8) octets (where n is the order of the
   curve).
*/


//
//
// // let searchData = data[Math.floor(Math.random() * data.length)];
let searchDataState = account[2];
// let searchData = transactionList[9];
// // // let searchData = data[data.length-1];
// //
// // tree.addLeaves("adam test");
// // tree.verifyData(transactionList[0]);
// transactionTrie.verifyData(searchData.from);
// // transactionTrie.remove(searchData.hash);
// searchData.to="xx";
// transactionTrie.verifyData(searchData.from);
// //
// console.log(account);
for (const a of account){
  if(!stateTrie.verifyData(a.address))
    break;
}
// // // tree.verifyData("test")
// transactionTree.viewTree();
stateTrie.viewTrie(3);
console.log(Object.keys(stateTrie.nodes));
// blocks[0].viewTree();
