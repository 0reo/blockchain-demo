const crypto = require("crypto");
const util = require('util');
const readline = require('readline');

const DEBUG = process.argv[3] || false;

const printDebug = (_msg) => {
  //assert prints on true, but DEBUG would be inverse of what we want if we used
  //it directly, so we check for the inverse of DEBUG instead
  DEBUG && console.log(_msg);
};

class PatriciaTrie {
  constructor() {
    this.nodes = {};
    this.dataSize = 1;
    this.branch = {
    }
  }

// insert data
// insert new data
// do we have a string match??
// if yes, rename


  add(_data, _field) {
    printDebug(`\n\nstart add: ${JSON.stringify(_data)}`);
    let hash = _field && _data[_field] ? _data[_field] : _data;
    const regex = new RegExp(`.{1,${this.dataSize}}`, 'g');
    hash = hash.match(regex)

    if (_data.DATA) _data = _data;

    let nodeStack = this.nodes;
    // create new node or select node if exists
    let searchChar = hash[0];
    let strStart = null;
    let strEnd = null;
    let strOldEnd = null;

    if (!Object.entries(nodeStack).length){
      nodeStack[hash.join('')] = {};
    } else {
      let nodeMatch = this.nodeSearch(nodeStack, searchChar, hash.join(''), true);
      printDebug(`RETURNED MATCH IS:`);
      printDebug(nodeMatch);
      nodeStack = nodeMatch;

    }

    if (nodeStack.END && nodeStack.END != hash.join('')){
      console.log(`Found conflicting hash, aborting add. ${nodeStack.END}`);
      return;
    }
    // nodeStack.END = hash;
    // console.log(`HERE, ${searchChar}, ${JSON.stringify(nodeStack)}`);
    if (this.addNodeData(this.nodes, _data, hash.join(''))){
      printDebug(`finished inserting data to trie`);
      printDebug(_data);
      printDebug(nodeStack);
    }

  }

  nodeSearch(_nodeStack, _searchChar, _inputHash, _restructure = false ){
    if (!Object.keys(_nodeStack).length || _searchChar.length >= 21){
      printDebug(`exiting search, faild ${_searchChar}, ${_inputHash}`);
       return _nodeStack;
     }

     printDebug(`\nsearching for ${_searchChar}, ${_inputHash}`);
     // console.log(_nodeStack);

     let newKeyStart = '';
     let matchingKey =  Object.keys(_nodeStack).find(elm => elm.startsWith(_searchChar));

    if (matchingKey && matchingKey !== _searchChar){

      if (matchingKey.length < _searchChar.length){
        printDebug(`removing beginning char from newKeyStart`)
        newKeyStart = newKeyStart.slice(1);//maybe use dynamic value rather than 1?
        _nodeStack = _nodeStack[_searchChar];
      } else {
        printDebug(`adding end char to newKeyStart`)
        newKeyStart = _inputHash.slice(0, _searchChar.length+1);
      }
      printDebug(`FOUND MATCH!: ${matchingKey}, ${_searchChar}, ${matchingKey !== _searchChar}`)

      return this.nodeSearch(_nodeStack, newKeyStart, _inputHash, _restructure)
      // printDebug(_nodeStack[matchingKey])
    }
     else if (!matchingKey && _searchChar.length <= 1 ){
       printDebug(`searchchar less than 1: ${_searchChar}`);

      return _restructure? this.restructure(_nodeStack, _inputHash) : _nodeStack;
    }
    else {

      // if (_nodeStack[matchingKey].PREFIX == 'BRANCH'){
      if (matchingKey == _searchChar && _nodeStack[matchingKey].PREFIX == 'BRANCH'){
        newKeyStart = _inputHash.slice(matchingKey.length, _searchChar.length+1);
        let newKeyEnd = _inputHash.slice(matchingKey.length - _inputHash.length);
        printDebug(`FOUND BRANCH MATCH!: ${matchingKey}, ${_searchChar}, ${newKeyStart}`)
        return this.nodeSearch(_nodeStack[matchingKey], newKeyStart, newKeyEnd, _restructure)
      }

      newKeyStart = _inputHash.slice(0, _searchChar.length-1) || _searchChar;
      printDebug(`MATCH IS..: ${newKeyStart}, ${_inputHash}, ${_searchChar}`);
      if (_restructure){
        let newKeyEnd = _inputHash.slice(newKeyStart.length - _inputHash.length);

        return this.restructure(_nodeStack, newKeyStart, newKeyEnd);
      }
      // console.log(_nodeStack[_searchChar]);
      // console.log(_nodeStack);
      // console.log('..'+_searchChar);
      return _nodeStack[_searchChar];
    }
    // console.log(_nodeStack[_searchChar]);
    // console.log('...'+_searchChar);
    // return _nodeStack[_searchChar];
  }

  restructure(_nodeStack, _newKeyStart, _newKeyEnd){
    let matchingKey =  Object.keys(_nodeStack).find(elm => elm.startsWith(_newKeyStart));
    let oldKeyEnd = null;
    printDebug(`restructuring`);
    printDebug(matchingKey);
    printDebug("new key start"+_newKeyStart);
    printDebug(_nodeStack);
    printDebug(_nodeStack[matchingKey]);


    let tmp = {};
    if (_newKeyEnd) tmp[ _newKeyEnd ] = {}; //placeholder for new data

    //make new branch
    if (matchingKey){
      if (_nodeStack[matchingKey].END){
        oldKeyEnd = matchingKey.slice(_newKeyStart.length) || matchingKey;
        // console.log(`OLD KEY END: ${oldKeyEnd}, ${matchingKey}, ${_newKeyStart}`);
        tmp.PREFIX = 'BRANCH';
      } else if (_nodeStack[matchingKey].PREFIX == "BRANCH"){
        oldKeyEnd = matchingKey.slice(matchingKey.length - _newKeyStart.length, matchingKey.length);
        // console.log(`THE NEW OLD KEY IS ${oldKeyEnd}, ${matchingKey.length - _newKeyStart.length}, ${matchingKey.length}, ${_newKeyStart.length}`);
      }
      tmp[ oldKeyEnd ] = {..._nodeStack[matchingKey]};
      // console.log(`tmp obj ${_newKeyStart}`);
      // console.log(tmp);
      delete Object.assign(_nodeStack, {[_newKeyStart]: _nodeStack[matchingKey] })[matchingKey]; //replace old entry with updated key

      if ('' in tmp){
        console.log("DIES EARLY 5", matchingKey, _newKeyStart, _newKeyEnd, oldKeyEnd);
        // console.log('stack', _nodeStack);
        console.log('tmp', tmp);
        process.exit();
      }
    }

    _nodeStack[_newKeyStart] = {...tmp};
    // console.log(`nodestack newKeyStart updated`);
    printDebug(`restructured! old hash(matchingKey): ${matchingKey},  start: ${_newKeyStart} end: ${_newKeyEnd}`)
    printDebug(_nodeStack[_newKeyStart]);


    // printDebug(`exiting suc ${_searchChar}`)
    return _nodeStack[_newKeyStart];
  }

  addNodeData(_nodeStack, _data, _hash){
    printDebug(`adding data to node..`)
    printDebug(_nodeStack)
    _nodeStack = this.nodeSearch(_nodeStack, _hash[0], _hash)

    // exit(1)
    if (_nodeStack.DATA){
      const end = _nodeStack.DATA.length - 1;
      if (_nodeStack.DATA[end].constructor.name == "Transaction" &&
        _nodeStack.DATA[end].id !== _data.id-1){
        console.log(`========Invalid nonce, exiting. ${_nodeStack.DATA[end].id}, ${_data.id}`)
        console.log(_nodeStack.DATA[end].constructor.name)
        // console.log(_nodeStack.DATA[end])
        console.log(_data)
        process.exit()

        return;
      }
      _nodeStack.DATA.push(_data)
    } else {
      _nodeStack.DATA = [_data];
    }
    _nodeStack.END = _hash;
    _nodeStack.PREFIX = 'LEAF';
    printDebug(`added data to nodestack`)
    printDebug(_nodeStack)
    printDebug(`${_hash}`)
    return true;
  }

  remove(_data){
    if (this.nodes == {}) {
      console.log(`no data stored in patricia trie`);
      return;
    }
    if (!this.verifyData(_data)) {
      console.log(`input data could not be verified, aborting removal`);
      return;
    }

    let lastChar = _data.slice(-1);
    let modStr = _data;

    while (modStr.length){

      let out = modStr.split('').reduce((nodes,char)=>{
        printDebug(`LOOPING ${char}, ${nodes && JSON.stringify(nodes[char])}, ${lastChar}`)
        return nodes && nodes[char]
      }, this.nodes);

      printDebug(`CHECKING NODE ${lastChar} FOR DELETION, ${JSON.stringify(out[lastChar])}, ${JSON.stringify(out)}`)
      if (out.END){
        if (out.END !== _data){
          delete out[lastChar];
          printDebug(`reached end node for another word, exiting. ${JSON.stringify(out[lastChar])}, ${_data}, ${lastChar}`);
          break;
        }

        printDebug(`DELETING END AND DATA NODE ${JSON.stringify(out)}, ${lastChar}`);
        delete out.END;
        delete out.DATA;
        printDebug(`out is now ${JSON.stringify(out)}, ${Object.values(out).length}`);
        if (Object.values(out).length){
          printDebug("FINISHING DUE TO SIBLINGS")
          break;
        };

      } else if (out[lastChar]){
        printDebug(`DELETING out[lastChar] ${JSON.stringify(out[lastChar])}, ${lastChar}`);
        delete out[lastChar];
        printDebug(`out is now ${JSON.stringify(out)}`);
      }
      lastChar = modStr.slice(-1);
      modStr = modStr.slice(0, -1);

    }
    console.log("Data successfully removed")
    return;

  }

  viewTrie(_depth=3) {
    console.log("\n\n\nFull trie");
    console.log(util.inspect(this.nodes, { depth: _depth, colors:true}));
    // util.inspect(this.nodes);
    // console.log(JSON.stringify(this.nodes, null, '\t'));
  }

  verifyData(_data, _showStack=false) {
    if (!_data){
      console.log(`empty input, exiting`);
      return;
    }
    if (this.nodes == {}) {
      console.log(`no data stored in patricia trie`);
      return;
    }

    printDebug(`starting search for ${_data}`);
    let nodeStack = this.nodeSearch(this.nodes, _data[0], _data)

    if (!nodeStack) {
      console.log(`${_data} not found in trie`);
      return;
    } else if (nodeStack.END && nodeStack.END == _data) {
      console.log(
        `finished searching for "${_data}" to trie, nodeStack end has value "${nodeStack.END}"`
      );
      if (_showStack)console.log(JSON.stringify(nodeStack.DATA, null, 2));
      return true;
    }
    console.log('how did we get here??');
    console.log(nodeStack, _data);
  }

  addLeaves(_data, _field) {
    const data = [].concat(_data);

    console.log(`adding ${data.length} items to trie, please wait`);
    for (const [i, item] of data.entries()) {
      this.add(item, _field);
      readline.clearLine();
      readline.cursorTo(process.stdout, 0);
      process.stdout.write(
        `${Math.round(((i + 1) / data.length) * 100)}% ` +
          ".".repeat(Math.round(((i + 1) / data.length) * 100))
      );
    }
    process.stdout.write(`\n\n`);
  }
}

module.exports = PatriciaTrie;
