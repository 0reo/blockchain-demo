var express = require('express');
const MerkleTree = require ("../bin/merkleTree");
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {

  const data = [];
  const blocks = [];

  const max = (req.query.max && req.query.max > 0) ? req.query.max : 1 + Math.floor(Math.random() * 10000);

  for (let i = 0; i < max; i++) {
    data.push({
      data:
      Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 5)
      }
    );

    if ((i+1) % 10 == 0) {
      let dataTree = new MerkleTree();
      dataTree.addLeaves(data.slice(-10));
      blocks.push(dataTree);
    }
  }

  // // tree.verifyData(transactionList[0]);


  res.json(data);
});

module.exports = router;
